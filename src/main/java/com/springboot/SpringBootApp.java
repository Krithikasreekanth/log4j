package com.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class SpringBootApp {
	private static final Logger logger = LoggerFactory.getLogger(SpringBootApp.class);
	

	     public static void main(String[] args) 
	     {
	    	 logger.info("Message logged at INFO level");
	         SpringApplication.run(SpringBootApp.class, args);
	     }
}
